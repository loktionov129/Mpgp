# Mpgp
## Multiplayer Game Platform

---
---

### Instructions for Windows

* Install .NET Core
[https://www.microsoft.com/net/learn/get-started/windows](https://www.microsoft.com/net/learn/get-started/windows)

---

* Go to the Package Manager Console
* Select default project Mpgp.DataAccess
* Run this command

```sh
Update-Database
```

---

* Copy default appsettings

```sh
copy ./tools/appsettings.default.json ./tools/appsettings.json
```

---
---

### Instructions for Linux/Mac

* Install .NET Core
[https://www.microsoft.com/net/learn/get-started/linuxubuntu](https://www.microsoft.com/net/learn/get-started/linuxubuntu)

---

* Configure

```sh
(cd ./src/Mpgp.DataAccess && dotnet add package Microsoft.EntityFrameworkCore.Design)
(cd ./src/Mpgp.DataAccess && dotnet add package Microsoft.EntityFrameworkCore.Tools)
(cd ./src/Mpgp.DataAccess && dotnet add package Microsoft.EntityFrameworkCore.Tools.DotNet)

(cd ./src/Mpgp.DataAccess && dotnet restore)
(cd ./src/Mpgp.DataAccess && dotnet ef database update)
```

---

* Build

```sh
cp ./tools/appsettings.default.json ./tools/appsettings.json
dotnet build ./src/Mpgp.RestApi/Mpgp.RestApi.csproj -c Release
```

---

* Startup

```sh
(cd ./src/Mpgp.RestApi/bin/Release/netcoreapp2.0 && dotnet Mpgp.RestApi.dll)
```